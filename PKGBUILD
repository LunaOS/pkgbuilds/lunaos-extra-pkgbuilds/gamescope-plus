_pkgname=gamescope
pkgname=gamescope-lunaos-nvidia
pkgver=3.13.19
pkgrel=5
pkgdesc='SteamOS session compositing window manager with added nvidia patches'
arch=(x86_64)
url=https://github.com/ChimeraOS/gamescope
license=(BSD)
conflicts=(gamescope)
provides=(gamescope)
depends=(
  gcc-libs
  glibc
  glm
  libavif
  libcap.so
  libdecor
  libdisplay-info.so
  libdrm
  libliftoff.so
  libpipewire-0.3.so
  libvulkan.so
  libwlroots.so
  libx11
  libxcb
  libxcomposite
  libxdamage
  libxext
  libxfixes
  libxkbcommon.so
  libxmu
  libxrender
  libxres
  libxtst
  libxxf86vm
  openvr
  sdl2
  vulkan-icd-loader
  wayland
  xorg-server-xwayland
  hwdata
  libinput
  xcb-util-wm
  xcb-util-errors
)
makedepends=(
  benchmark
  git
  glslang
  meson
  ninja
  vulkan-headers
  wayland-protocols
  cmake
)

install=$pkgname.install

_tag=3e14ef9c37266b19ba77fbef467d1b8a77d827f2
source=("git+https://github.com/ValveSoftware/gamescope.git#tag=${_tag}"
        "git+https://github.com/Joshua-Ashton/reshade.git"
        "git+https://github.com/KhronosGroup/SPIRV-Headers.git"
        "git+https://github.com/Joshua-Ashton/GamescopeShaders.git#tag=v0.1"
        "crashfix.patch"
        "chimeraos.patch"
        "legion_go.patch"
        "touch_gestures_env.patch"
        "0001-Revert-rendervulkan-Get-a-general-graphics-compute-q.patch"
        "0002-fix-error-due-to-removed-general-command-pool.patch"
        "0003-oops.patch"
        "0004-fix-removed-general-queue.patch"
		"fix-build-with-openvr-2.patch"
        )

b2sums=('88be5615a626ad2ed57053dbb8e9589d7c661996e5945aee6483f09f7a0ccffa1d79a69ed16542c46aa556f408e5531b86e756c9d8d6569adb31e2b4404f28e8'
        'SKIP'
        'SKIP'
        'ca268553bc3dacb5bd19553702cd454ea78ed97ab39d4397c5abf9a27d32633b63e0f7f7bf567b56066e6ecd979275330e629ba202a6d7721f0cd8166cd110dd'
        'e45d9a03102c92eaadf374cf4fd452d840fae145ce3efdce8b6e7f3e2c94f784282b1017cdb33953d692e59359ebe2813da19aa888a736ac2286f4cb8b884038'
        'db2ff02be12f496da06c01a05e203a6370eb2e71270be21d253dcabb137fd24a5664cf68677fc940065cf7c82b9927caf21577840ca72c0db6b61794c66c133c'
        'd6c99ce3054a47680d7f9c0193cd6b65426fae9459ecc1a4ee7be5b8e6ffe1beef0082db68b7417b0da3e85f7efc64136d4683107f2229386aa2e5b6ab91b001'
        '91badc50b767a140f00144313fef5670ad6c78283bd0380a083c55162d9b7a28107c6bee68eae7e85613a60610c14d330dc75b058ca68850780c15362f3ef30e'
        'f0a7562a7825df16878f733669056dc5ecda36e62afc945e745b6746ee7ecf6983d4659b0061f73852165537479fac68ecd59913b2ff6fdc5ac18ff23d891d97'
        '6c5e3cc88cd4882ea6a1a1afa7a47d93dcc665128d7fa125d619440303530f4f774046ae157134521b15df022fc389241b7a58cf386aec53de98a67983a3589a'
        '62a07d81a63d390be1c4bc838b4dea920c41efb37ca6aff919696f8ca104b688eeeea896fb2a4706b20f487578864bbaee4f09fe4b764b5a17cc6384bed1cd38'
        'bde7af1325b29ee4456272cf04c999d9248d320334d1794ebf3181962b93f8d30c31e308d73b4d67f592729bda59763f790890b7d82e7d8e951ed2f8415d9b36'
        'd754457907bf8e3e99f9828b3f765a246a245748c5a7e0fc0d9d3da1ac3b0be5097db71fe8690d10e5f70dab15a05b3aed52ce6c9c4eedc52ec01b9f056dc482')

prepare() {

  # apply nvidia-fix patchs from
  # https://github.com/sharkautarch/gamescope/tree/nvidia-fix
  patch -d "$srcdir/$_pkgname" -p1 -i "$srcdir/0001-Revert-rendervulkan-Get-a-general-graphics-compute-q.patch"
  patch -d "$srcdir/$_pkgname" -p1 -i "$srcdir/0002-fix-error-due-to-removed-general-command-pool.patch"
  patch -d "$srcdir/$_pkgname" -p1 -i "$srcdir/0003-oops.patch"
  patch -d "$srcdir/$_pkgname" -p1 -i "$srcdir/0004-fix-removed-general-queue.patch"

  # apply chimeraos patchs from
  # https://github.com/ChimeraOS/gamescope/
  patch -d "$srcdir/$_pkgname" -p1 -i "$srcdir/chimeraos.patch"
  patch -d "$srcdir/$_pkgname" -p1 -i "$srcdir/crashfix.patch"
  patch -d "$srcdir/$_pkgname" -p1 -i "$srcdir/legion_go.patch"
  patch -d "$srcdir/$_pkgname" -p1 -i "$srcdir/touch_gestures_env.patch"

  # apply openvr patch
  patch -d "$srcdir/$_pkgname" -p1 -i "$srcdir/fix-build-with-openvr-2.patch"

  cd "$srcdir/$_pkgname"
  meson subprojects download
  git submodule init src/reshade
  git config submodule.src/reshade.url ../reshade
  git submodule init thirdparty/SPIRV-Headers
  git config submodule.thirdparty/SPIRV-Headers.url ../SPIRV-Headers
  git -c protocol.file.allow=always submodule update
}

pkgver() {
  cd gamescope
  git describe --tags | sed 's/\-/\./g'
}

build() {
  arch-meson gamescope build \
    -Dpipewire=enabled \
    -Denable_gamescope=true \
    -Denable_gamescope_wsi_layer=true \
    -Denable_openvr_support=true \
    -Dforce_fallback_for=stb,glm,vkroots
  meson compile -C build
}

package() {

  install -d "$pkgdir"/usr/share/gamescope/reshade
  cp -r "$srcdir"/GamescopeShaders/* "$pkgdir"/usr/share/gamescope/reshade/
  chmod -R 655 "$pkgdir"/usr/share/gamescope

  DESTDIR="${pkgdir}" meson install -C build \
    --skip-subprojects
  install -Dm 644 gamescope/LICENSE -t "${pkgdir}"/usr/share/licenses/gamescope/
}
